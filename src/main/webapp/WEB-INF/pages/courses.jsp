<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
 
<%@page pageEncoding="UTF-8"%><%@ page import="java.util.*" %>
<html>
<head>
<style>
    table{
        border: 1px solid;
    }
</style>
</head>

<body>
<table>
    <tr><th colspan=3>Courses</th></tr>
    <tr>
        <th><b>Course</b></th><th><b>Students</b></th><th><b>Deleted</b></th>
    </tr>
    <c:forEach var="course" items="${courses}">
        <tr><td>(${course.id})${course.courseName}</td><td>
            <c:forEach var="student" items="${course.students}">
                ${student.firstName} ${student.lastName};
            </c:forEach>
        </td><td><a href="/courses/delete/${course.id}">delete ${course.courseName}</a></td></tr>
    </c:forEach>
    <tr><td>&nbsp;</td></tr>
    <tr><td colspan="3">Add a Course</td></tr>
    <tr><form:form action="/courses/insert" method="post">
        <td><label>Course Name:</label><input type="text" id="courseName" name="courseName"/></td>
        <td><input type="submit" id="submit"/></td>
    </form:form>
    </tr>
</table>
That's all for today fox!<br><br>

</body>
</html>