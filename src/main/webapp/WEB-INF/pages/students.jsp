<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
 
<%@page pageEncoding="UTF-8"%><%@ page import="java.util.*" %>
<html>
<head>
<style>
table {
	border: 1px solid;
}
table th{
    color: #312be2;
}
</style>
</head>

<body>
<table>
    <tr>
        <td>
    <!-- Student -->
            <table width="500px" height="300px">
                <tr><th colspan=3>Students</th></tr>
                <tr>
                    <td><b>Student</b></td><td><b>Courses</b></td><td><b>Deleted</b></td>
                </tr>
	                <c:forEach var="student" items="${students}">
	    				<tr><td>(${student.id})${student.firstName} ${student.lastName}</td><td>
                            <c:forEach var="course" items="${student.courses}">
                                ${course.courseName}(${course.id});
                            </c:forEach>
                        </td><td><a href="/students/delete/${student.id}">delete ${student.firstName}</a></td></tr>
	    			</c:forEach>   
				<tr><td>&nbsp;</td></tr>
                <tr><td colspan="3"><b>Add a student</b></td></tr>
                <tr><form:form  action="/students/insert" method="post">
                    <td><label>First Name</label><input type="text" name="firstName"/></td>
                    <td><label>Last Name</label><input type="text" name="lastName"/></td>
                    <td><input type="submit"/></td>
                    </form:form>
                </tr>
            </table>
        </td>
        <td>
    <!-- Course -->
            <table width="500px" height="300px">
                <tr><th colspan=3>Courses</th></tr>
                <tr>
                    <td><b>Course</b></td><td><b>Students</b></td><td><b>Deleted</b></td>
                </tr>
                <c:forEach var="course" items="${courses}">
                    <tr><td>(${course.id})${course.courseName}</td><td>
                        <c:forEach var="student" items="${course.students}">
                            ${student.firstName} ${student.lastName};
                        </c:forEach>
                    </td><td><a href="/courses/delete/${course.id}">delete ${course.courseName}</a></td></tr>
                </c:forEach>
                <tr><td>&nbsp;</td></tr>
                <tr><td colspan="3"><b>Add a Course</b></td></tr>
                <tr><form:form action="/courses/insert" method="post">
                    <td><label>Course Name</label><input type="text" name="courseName"/></td>
                    <td><input type="submit" /></td>
                </form:form>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    <td>
        <table width="500px">
            <tr><th colspan=3>Add Enrollment</th></tr>
            <tr>
                <td><b>Student</b></td><td><b>Course</b></td><td>&nbsp;</td>
            </tr>
            <tr>
                <form:form action="/enrollment/add" method="post">
                    <td><label>Student</label></td>
                    <td>
                        <select name="en_student">
                            <c:forEach var="student" items="${students}">
                                <option value="${student.id}">${student.firstName} ${student.lastName}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td>
                        <select name="en_course">
                            <c:forEach var="course" items="${courses}">
                                <option value="${course.id}">${course.courseName}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td><input type="submit" /></td>
                </form:form>
            </tr>


        </table>
    </td>
        <td>
            <table width="500px">
                <tr><th colspan=3>Remove Enrollment</th></tr>
                <tr>
                    <td><b>Student</b></td><td><b>Course</b></td><td>&nbsp;</td>
                </tr>
                <tr>
                    <form:form action="/enrollment/delete" method="post">
                        <td><label>Student</label></td>
                        <td>
                            <select name="en_student">
                                <c:forEach var="student" items="${students}">
                                    <option value="${student.id}">${student.firstName} ${student.lastName}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td>
                            <select name="en_course">
                                <c:forEach var="course" items="${courses}">
                                    <option value="${course.id}">${course.courseName}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td><input type="submit"/></td>
                    </form:form>
                </tr>


            </table>
        </td>
</tr>
</table>
<br>
That's all for today fox!<br><br>

</body>
</html>