CREATE TABLE `STUDENTS` (
    `student_id` int(6) NOT NULL AUTO_INCREMENT,
    `first_name` varchar(40) NOT NULL,
    `last_name` varchar(40) NOT NULL,
    PRIMARY KEY (`student_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

insert into students(first_name, last_name) values('Nir','Alfasi');
insert into students(first_name, last_name) values('David','Wang');

CREATE TABLE `COURSES` (
    `course_id` int(6) NOT NULL AUTO_INCREMENT,
    `course_name` varchar(40) NOT NULL,
    PRIMARY KEY (`course_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

insert into COURSES(course_name) values('Math');
insert into COURSES(course_name) values('CS');
insert into COURSES(course_name) values('Spring');


CREATE TABLE `ENROLLMENTS` (
    `id` int(6) NOT NULL AUTO_INCREMENT,
    `course_id` int(6) NOT NULL,
    `student_id` int(6) NOT NULL,
    FOREIGN KEY (course_id)
        REFERENCES COURSES(course_id)
        ON DELETE CASCADE,
    FOREIGN KEY (student_id)
        REFERENCES STUDENTS(student_id)
        ON DELETE CASCADE,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

insert into ENROLLMENTS(course_id, student_id) values(12,15);
insert into ENROLLMENTS(course_id, student_id) values(12,16);
insert into ENROLLMENTS(course_id, student_id) values(12,17);
insert into ENROLLMENTS(course_id, student_id) values(14,15);
insert into ENROLLMENTS(course_id, student_id) values(14,16);
