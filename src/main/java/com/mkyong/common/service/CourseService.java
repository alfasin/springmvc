package com.mkyong.common.service;
/**
 * User: alfasin
 * Date: 8/22/13
 */
import com.mkyong.common.dao.CourseDaoImpl;
import com.mkyong.common.model.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;



@Transactional
@Service
public class CourseService {

    @Autowired
    CourseDaoImpl dao;

    public Course getCourse(Integer id) {
        return dao.getCourse(id);
    }

    public Course insertCourse(Course course) {
        return dao.insertCourse(course);
    }

    public void updateCourse(Course course) {
        dao.updateCourse(course);
    }

    public void deleteCourse(Integer id) {
        dao.deleteCourse(id);
    }
}
