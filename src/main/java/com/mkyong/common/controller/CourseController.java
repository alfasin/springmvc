package com.mkyong.common.controller;

import com.mkyong.common.dao.CourseDaoImpl;
import com.mkyong.common.dao.StudentDaoImpl;
import com.mkyong.common.model.Course;
import com.mkyong.common.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedList;
import java.util.List;

/**
 * User: alfasin
 * Date: 8/25/13
 */
@Controller
@RequestMapping("/courses")
public class CourseController {

    @Autowired
    CourseDaoImpl cd = null;
    @Autowired
    StudentDaoImpl sd = null;

    @RequestMapping(method = RequestMethod.GET)
    public String getAllCourses(ModelMap model) {
        saveParamsToModel(model);
        return "students";
    }

    @RequestMapping(value="/{courseId}",method = RequestMethod.GET)
    public String getCourse(ModelMap model, @PathVariable Integer courseId) {
        List<Course> courses = new LinkedList<Course>();
        courses.add(cd.getCourse(courseId));
        saveParamsToModel(model);
        return "students";
    }

    @RequestMapping(value="/delete/{courseId}",method = RequestMethod.GET)
    public String deleteStudent(ModelMap model, @PathVariable Integer courseId) {
        try{cd.deleteCourse(courseId);} catch(Exception e){}
        saveParamsToModel(model);
        return "students";
    }

    @RequestMapping(value="/insert",method = RequestMethod.POST)
    public String addCourse(ModelMap model, HttpServletRequest request) {
        String courseName = request.getParameter("courseName");
        Course course = new Course(courseName);
        try{cd.insertCourse(course);} catch(Exception e){}
        saveParamsToModel(model);
        return "students";
    }

    private void saveParamsToModel(ModelMap model) {
        List<Student> students = sd.getAllStudents();
        model.addAttribute("students", students);
        List<Course> courses = cd.getAllCourses();
        model.addAttribute("courses", courses);
    }
}
