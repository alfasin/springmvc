package com.mkyong.common.controller;

import com.mkyong.common.dao.CourseDaoImpl;
import com.mkyong.common.dao.EnrollmentDaoImpl;
import com.mkyong.common.dao.StudentDaoImpl;
import com.mkyong.common.model.Course;
import com.mkyong.common.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * User: alfasin
 * Date: 8/28/13
 */
@Controller
@RequestMapping("/enrollment")
public class EnrollmentController {

    @Autowired
    EnrollmentDaoImpl ed = null;
    @Autowired
    StudentDaoImpl sd = null;
    @Autowired
    CourseDaoImpl  cd = null;


    @RequestMapping(value="/delete",method = RequestMethod.POST)
    public String deleteEnrollment(ModelMap model,
                                @RequestParam("en_student") Integer studentId,
                                @RequestParam("en_course") Integer courseId) {
        try{ed.remove(studentId, courseId);} catch(Exception e){}
        saveParamsToModel(model);
        return "students";
    }

    @RequestMapping(value="/add",method = RequestMethod.POST)
    public String addEnrollment(ModelMap model,
                                @RequestParam("en_student") Integer studentId,
                                @RequestParam("en_course") Integer courseId) {
        try{ed.insert(studentId, courseId);} catch(Exception e){}
        saveParamsToModel(model);
        return "students";
    }

    private void saveParamsToModel(ModelMap model) {
        List<Student> students = sd.getAllStudents();
        model.addAttribute("students", students);
        List<Course> courses = cd.getAllCourses();
        model.addAttribute("courses", courses);
    }
}
