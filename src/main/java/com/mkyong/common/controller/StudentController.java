package com.mkyong.common.controller;
/**
 * 
 * @author alfasin
 * 
 */
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.mkyong.common.dao.CourseDaoImpl;
import com.mkyong.common.dao.StudentDaoImpl;
import com.mkyong.common.model.Course;
import com.mkyong.common.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.mkyong.common.dao.UserDaoImpl;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/students")
public class StudentController {
		
	@Autowired
    StudentDaoImpl sd = null;
    @Autowired
    CourseDaoImpl  cd = null;

	@RequestMapping(method = RequestMethod.GET)
	public String getAllStudents(ModelMap model) {
        saveParamsToModel(model);
        return "students";
	}

    @RequestMapping(value="/{studentId}",method = RequestMethod.GET)
	public String getStudent(ModelMap model, @PathVariable Integer studentId) {
		List<Student> students = new LinkedList<Student>();
        students.add(sd.getStudent(studentId));
        saveParamsToModel(model);
		return "students";
	}

    @RequestMapping(value="/delete/{studentId}",method = RequestMethod.GET)
    public String deleteStudent(ModelMap model, @PathVariable Integer studentId) {
        try{sd.deleteStudent(studentId);} catch(Exception e){}
        saveParamsToModel(model);
        return "students";
    }

    @RequestMapping(value="/insert",method = RequestMethod.POST)
    public String addStudent(ModelMap model,
                             @RequestParam("firstName") String firstName,
                             @RequestParam("lastName") String lastName) {
        Student student = new Student(firstName, lastName);
        try{sd.insertStudent(student);} catch(Exception e){}
        saveParamsToModel(model);
        return "students";
    }

    private void saveParamsToModel(ModelMap model) {
        List<Student> students = sd.getAllStudents();
        model.addAttribute("students", students);
        List<Course> courses = cd.getAllCourses();
        model.addAttribute("courses", courses);
    }
}
