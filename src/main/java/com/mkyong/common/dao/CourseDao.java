package com.mkyong.common.dao;

import com.mkyong.common.model.Course;

/**
 * User: alfasin
 * Date: 8/22/13
 */
public interface CourseDao {

    public Course insertCourse(Course course);
    public Course getCourse(Integer id);
    public Course getCourse(String courseName);
    public void updateCourse(Course course);
    public void deleteCourse(Integer id);

}
