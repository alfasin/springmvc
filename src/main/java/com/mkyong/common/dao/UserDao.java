package com.mkyong.common.dao;
/**
 * 
 * @author alfasin
 * 
 */
import com.mkyong.common.model.User;

public interface UserDao {  
	 public User getUserDetails(String userId);  
	 public String saveUserDetails(User user);  	
}
