package com.mkyong.common.dao;

import com.mkyong.common.model.Student;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * User: alfasin
 * Date: 8/21/13
 */

@Repository
public class StudentDaoImpl implements StudentDao {

    @Autowired
    SessionFactory session;

    @Override
    public Student getStudent(Integer id) {
        Transaction tx = null;
        Student res = null;
        try{
            tx = session.getCurrentSession().beginTransaction();
            res =  (Student) session.getCurrentSession().get(Student.class, id);
            tx.commit();
        }
        catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public Student insertStudent(Student student) {
        Transaction tx = null;
        Student res = null;
        try{
            tx = session.getCurrentSession().beginTransaction();
            Integer id = (Integer) session.getCurrentSession().save(student);
            res = (Student) session.getCurrentSession().get(Student.class, id);
            tx.commit();
        }
        catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public void updateStudent(Student student) {
        Transaction tx = null;
        try{
            tx = session.getCurrentSession().beginTransaction();
            session.getCurrentSession().update(student);
            tx.commit();
        }
        catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }
    }

    @Override
    public void deleteStudent(Integer id) {
        Transaction tx = null;
        try{
            tx = session.getCurrentSession().beginTransaction();
            Student student = (Student) session.getCurrentSession().get(Student.class, id);
            session.getCurrentSession().delete(student);
            tx.commit();
        }
        catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }
    }

    public List<Student> getAllStudents() {
        Transaction tx = null;
        List<Student> res = null;
        try{
            tx = session.getCurrentSession().beginTransaction();
            res = (List<Student>) session.getCurrentSession().createQuery("from Student order by firstName asc").list();
            tx.commit();
        }
        catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }
        return res;
    }
}
