package com.mkyong.common.dao;
/**
 * User: alfasin
 * Date: 8/22/13
 */
import com.mkyong.common.model.Course;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class CourseDaoImpl implements CourseDao {

    @Autowired
    SessionFactory session;

    @Override
    public Course getCourse(Integer id) {
        Transaction tx = null;
        Course res = null;
        try{
            tx = session.getCurrentSession().beginTransaction();
            res = (Course) session.getCurrentSession().get(Course.class, id);
            tx.commit();
        }
        catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }
        return res;
    }
    @Override
    public Course getCourse(String courseName) {
        Transaction tx = null;
        Course res = null;
        try{
            tx = session.getCurrentSession().beginTransaction();
            List<Course> result = (List<Course>) session.getCurrentSession().createQuery("from Course where course_name = '"+courseName+"'").list();
            res = result.get(0);
            tx.commit();
        }
        catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public Course insertCourse(Course course) {
        Transaction tx = null;
        Course res = null;
        try{
            tx = session.getCurrentSession().beginTransaction();
            Integer id = (Integer) session.getCurrentSession().save(course);
            res = (Course) session.getCurrentSession().get(Course.class, id);
            tx.commit();
        }
        catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public void updateCourse(Course course) {
        Transaction tx = null;
        try{
            tx = session.getCurrentSession().beginTransaction();
            session.getCurrentSession().save(course);
            tx.commit();
        }
        catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }
    }

    @Override
    public void deleteCourse(Integer id) {
        Transaction tx = null;
        try{
            tx = session.getCurrentSession().beginTransaction();
            Course course = (Course) session.getCurrentSession().get(Course.class, id);
            session.getCurrentSession().delete(course);
            tx.commit();
        }
        catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }
    }

    public List<Course> getAllCourses() {
        Transaction tx = null;
        List<Course>  res = null;
        try{
            tx = session.getCurrentSession().beginTransaction();
            res = (List<Course>) session.getCurrentSession().createQuery("from Course order by courseName asc").list();
            tx.commit();
        }
        catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }
        return res;
    }
}
