package com.mkyong.common.dao;
/**
 * 
 * @author alfasin
 *
 */
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import com.mkyong.common.model.User;

@Component
public class UserDaoImpl implements UserDao{

    JdbcTemplate jdbcTemplate;    
    @Autowired
    BasicDataSource dataSource;

    
	@Override
	public User getUserDetails(final String username) {
		String query = "SELECT * FROM USERS WHERE USERNAME=?";
		jdbcTemplate = new JdbcTemplate(dataSource);
		return jdbcTemplate.queryForObject(query, new UserMapper(), username); 
	}
		
	private static final class UserMapper implements RowMapper<User> { 
		public User mapRow(ResultSet rs, int rowNum) throws SQLException { 
			User user = new User(); 
			user.setId(rs.getString("id"));
			user.setFirstName(rs.getString("first_name"));
			user.setLastName(rs.getString("last_name"));
			user.setUsername(rs.getString("username"));
			user.setPassword(rs.getString("passwod")); 			 
			return user; 
		} 		
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<String> findAll(){		 
		String sql = "SELECT FIRST_NAME, LAST_NAME FROM USERS";	 
		jdbcTemplate = new JdbcTemplate(dataSource);
		List users = jdbcTemplate.queryForList(sql);
		ArrayList<String> res = new ArrayList<String>();
		for (HashMap<String, String> map : (List<HashMap<String, String>>)users) {
            String fullname = "";
            for (String val : map.values()) {
                //System.out.print(val+" ");
                fullname += val+" ";
            }
            //System.out.println();
            res.add(fullname);
        }
		return res;
	}

	@Override
	public String saveUserDetails(User user) {
		// TODO Auto-generated method stub
		return null;
	}
	

}
