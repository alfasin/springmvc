package com.mkyong.common.dao;

import com.mkyong.common.model.Student;

/**
 * User: alfasin
 * Date: 8/21/13
 */
public interface StudentDao {

    public Student getStudent(Integer id);
    public Student insertStudent(Student student);
    public void updateStudent(Student student);
    public void deleteStudent(Integer id);

}
