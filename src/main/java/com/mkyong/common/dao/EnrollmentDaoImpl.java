package com.mkyong.common.dao;

import com.mkyong.common.model.Enrollment;
import com.mkyong.common.model.Student;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * User: alfasin
 * Date: 8/28/13
 */
@Repository
public class EnrollmentDaoImpl {

    @Autowired
    SessionFactory session;

    public Enrollment getEnrollment(Integer studentId, Integer courseId) {
        Transaction tx = null;
        Enrollment res = null;
        try{
            tx = session.getCurrentSession().beginTransaction();
            String sql = "from Enrollment where studentId = "+studentId+" and courseId = "+courseId;
            System.out.println("sql = "+sql);
            List<Enrollment> result = (List<Enrollment>) session.getCurrentSession().createQuery(sql).list();
            res = result.get(0);
            tx.commit();
        }
        catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }
        return res;
    }

    public void remove(Integer studentId, Integer courseId) {
        Transaction tx = null;
        Enrollment enrollment = getEnrollment(studentId, courseId);
        try{
            tx = session.getCurrentSession().beginTransaction();
            enrollment.print();//System.out.println("Going to delete enrollment: " + enrollment.studentId + " " + enrollment.courseId);
            session.getCurrentSession().delete(enrollment);
            System.out.println("After delete enrollment: "+enrollment.studentId+" "+enrollment.courseId);
            tx.commit();
        }
        catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }
    }

    public void insert(Integer studentId, Integer courseId) {
        Transaction tx = null;
        Enrollment enrollment = new Enrollment(studentId, courseId);
        try{
            tx = session.getCurrentSession().beginTransaction();
            session.getCurrentSession().save(enrollment);
            tx.commit();
        }
        catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }
    }
}
