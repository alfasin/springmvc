package com.mkyong.common.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name="STUDENTS")
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="STUDENT_ID", nullable = false)
    @XmlElement(required = false)
    private Integer id;

    @Column(name="FIRST_NAME", nullable = false)
    @XmlElement
    public String firstName;

    @Column(name="LAST_NAME", nullable = false)
    @XmlElement
    public String lastName;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, targetEntity=Course.class)
    @JoinTable(name = "ENROLLMENTS", catalog = "spring", joinColumns = {
            @JoinColumn(name = "STUDENT_ID", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "COURSE_ID",
                    nullable = false, updatable = false) })
    private Set<Course> courses = new HashSet<Course>(0);

    public Student(){}

    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<Course> getCourses(){
        return this.courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }

    public void print(){
        System.out.println("id: "+id+"; "+firstName+" "+lastName);
        for(Course c: courses){
            System.out.print(c.getCourseName()+", ");
        }
        System.out.println();
    }
}
