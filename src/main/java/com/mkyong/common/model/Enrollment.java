package com.mkyong.common.model;

import javax.persistence.*;

/**
 * User: alfasin
 * Date: 8/28/13
 */
@Entity
@Table(name="ENROLLMENTS")
public class Enrollment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID", nullable = false)
    private Integer id;

    @Column(name="STUDENT_ID", nullable = false)
    public Integer studentId;

    @Column(name="COURSE_ID", nullable = false)
    public Integer courseId;

    public Enrollment(Integer studentId, Integer courseId) {
        this.studentId = studentId;
        this.courseId = courseId;
    }

    public Enrollment(){}

    public void print(){
        System.out.println("StudentId: "+studentId+"; courseId:"+courseId);
    }
}
