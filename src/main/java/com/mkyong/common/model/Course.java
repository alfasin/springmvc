package com.mkyong.common.model;

/**
 * User: alfasin
 * Date: 8/21/13
 */
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name="COURSES")
@XStreamAlias("course")
@XmlRootElement(name = "course")
@XmlAccessorType(XmlAccessType.FIELD)
public class Course implements Serializable {

    private static final long serialVersionUID = 2L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="COURSE_ID", nullable = false)
    @XmlElement(required = false)
    private Integer id;

    @Column(name="COURSE_NAME", nullable = false)
    @XmlElement
    private String courseName;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH},
            fetch = FetchType.EAGER,
            mappedBy = "courses",
            targetEntity = Student.class)
    private Set<Student> students = new HashSet<Student>(0);

    public Course(String name){
        this.courseName = name;
    }

    public Course(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

}
